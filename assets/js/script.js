let student1 = {
    name: "Shawn Michaels",
    birthday: "May 5, 2003",
    age: 18,
    isEnrolled: true,
    classes: ["Philosphy 101", "Social Sciences 201"]
}
let student2 = {
    name: "Steve Austin",
    birthday: "June 15, 2001",
    age: 20,
    isEnrolled: true,
    classes: ["Philosphy 401", "Natural Sciences 402"]
}
const introduce = (student) => {
    console.log(`Hi I'm ${student.name}. I am was born on ${student.birthday}, I am ${student.age}. I am enrolled in ${student.classes}`);
}
const classes1 = [course1, course2] = student1.classes;
const classes2 = [course3, course4] = student2.classes;

introduce(student1);
introduce(student2);

const getCube = (num) => Math.pow(num, 3)
let cube = getCube(3);
console.log(cube)

let numArr = [15, 16, 32, 21, 21, 2]

numArr.forEach(num => console.log(num));

let numSquared = numArr.map(num => Math.pow(num, 2));
console.log(numSquared);

class Car {
    constructor(brand, model, year) {
        this.brand = brand;
        this.name = name;
        this.year = year;
    }
}
let carOne = new Car('Mitsubishi', 'Evo', '2007');
let carTwo = new Car('Suzuki', 'Jimny', '2020');

console.log(carOne);
console.log(carTwo);